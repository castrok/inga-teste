<?php

namespace App\Models;

use App\Models\Base\CustomersEmploye as BaseCustomersEmploye;

class CustomersEmploye extends BaseCustomersEmploye
{
	protected $fillable = [
		'employe',
		'customer'
	];
}
