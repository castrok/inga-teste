<?php

namespace App\Models;

use App\Models\Base\Job as BaseJob;

class Job extends BaseJob
{
	protected $fillable = [
		'job_name',
		'job_customer',
		'job_employe'
	];
}
