<?php

namespace App\Models;

use App\Models\Base\Employe as BaseEmploye;

class Employe extends BaseEmploye
{
	protected $fillable = [
		'employe_name'
	];
}
