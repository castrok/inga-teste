<?php

namespace App\Models;

use App\Models\Base\Service as BaseService;

class Service extends BaseService
{
	protected $fillable = [
		'service_name'
	];
}
