<?php

namespace App\Models;

use App\Models\Base\Customer as BaseCustomer;

class Customer extends BaseCustomer
{
	protected $fillable = [
		'customer_name'
	];
}
