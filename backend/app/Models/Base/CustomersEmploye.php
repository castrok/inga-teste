<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models\Base;

use App\Models\Customer;
use App\Models\Employe;
use Illuminate\Database\Eloquent\Model;

/**
 * Class CustomersEmploye
 * 
 * @property int $employe
 * @property int $customer
 * 
 *
 * @package App\Models\Base
 */
class CustomersEmploye extends Model
{
	protected $table = 'customers_employes';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'employe' => 'int',
		'customer' => 'int'
	];

	public function customer()
	{
		return $this->belongsTo(Customer::class, 'customer');
	}

	public function employe()
	{
		return $this->belongsTo(Employe::class, 'employe');
	}
}
