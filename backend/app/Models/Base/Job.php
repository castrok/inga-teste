<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models\Base;

use App\Models\Customer;
use App\Models\Employe;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Job
 * 
 * @property int $job_id
 * @property string $job_name
 * @property int $job_customer
 * @property int $job_employe
 * @property Carbon $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 * 
 * @property Customer $customer
 * @property Employe $employe
 *
 * @package App\Models\Base
 */
class Job extends Model
{
	use SoftDeletes;
	protected $table = 'jobs';
	protected $primaryKey = 'job_id';

	protected $casts = [
		'job_customer' => 'int',
		'job_employe' => 'int'
	];

	public function customer()
	{
		return $this->belongsTo(Customer::class, 'job_customer');
	}

	public function employe()
	{
		return $this->belongsTo(Employe::class, 'job_employe');
	}
}
