<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models\Base;

use App\Models\Customer;
use App\Models\Job;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Employe
 * 
 * @property int $employe_id
 * @property string $employe_name
 * @property Carbon $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 * 
 * @property Collection|Customer[] $customers
 * @property Collection|Job[] $jobs
 *
 * @package App\Models\Base
 */
class Employe extends Model
{
	use SoftDeletes;
	protected $table = 'employes';
	protected $primaryKey = 'employe_id';

	public function customers()
	{
		return $this->belongsToMany(Customer::class, 'customers_employes', 'employe', 'customer');
	}

	public function jobs()
	{
		return $this->hasMany(Job::class, 'job_employe');
	}
}
