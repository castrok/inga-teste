<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models\Base;

use App\Models\Employe;
use App\Models\Job;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Customer
 * 
 * @property int $customer_id
 * @property string $customer_name
 * @property Carbon $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 * 
 * @property Collection|Employe[] $employes
 * @property Collection|Job[] $jobs
 *
 * @package App\Models\Base
 */
class Customer extends Model
{
	use SoftDeletes;
	protected $table = 'customers';
	protected $primaryKey = 'customer_id';

	public function employes()
	{
		return $this->belongsToMany(Employe::class, 'customers_employes', 'customer', 'employe');
	}

	public function jobs()
	{
		return $this->hasMany(Job::class, 'job_customer');
	}
}
