<?php

namespace App\Http\Controllers;

use App\Models\Employe;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class EmployesController extends Controller {
    /**
     * Display a listing of the resource.
     * @author Gilson Vieira Castro Júnior <castrok@live.com / +55 (61) 9 9209-6171>
     * @return JsonResponse
     */
    public function index(): JsonResponse {
        try {
            $employes = Employe::all();
        }
        catch (Exception $exception) {
            report($exception);
            return response()->json($exception,500);
        }
        return response()->json($employes);
    }

    /**
     * Store a newly created resource in storage.
     * @author Gilson Vieira Castro Júnior <castrok@live.com / +55 (61) 9 9209-6171>
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse {
        try {
            $employe = Employe::insert([
                'employe_name' => $request->employe_name,
                'employe_birth' => $request->employe_birth
            ]);
        }
        catch (Exception $exception) {
            report($exception);
            return response()->json($exception,500);
        }
        return response()->json($employe);
    }

    /**
     * Display the specified resource.
     * @param int $id
     * @return JsonResponse
     * @author Gilson Vieira Castro Júnior <castrok@live.com / +55 (61) 9 9209-6171>
     */
    public function show(int $id): JsonResponse {
        try{
            $employe = Employe::find($id);
            /*Employe::query()
                            ->join('jobs', 'jobs.job_employe', 'employes.employe_id')
                            ->join('employes', 'employes.employe_id', 'jobs.job_employe')
                            ->join('services', 'services.service_id', 'jobs.job_service')
                            ->where('employes.employe_id', $id)
                            ->get()->first();
            */
        }
        catch (Exception $exception) {
            report($exception);
            return response()->json($exception,500);
        }
        return response()->json($employe);
    }

    /**
     * Return the specified resource for editing.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function edit(int $id): JsonResponse {
        try{
            $employes = Employe::find($id);
        }
        catch (Exception $exception) {
            report($exception);
            return response()->json($exception,500);
        }
        return response()->json($employes);
    }

    /**
     * Update the specified resource in storage.
     * @author Gilson Vieira Castro Júnior <castrok@live.com / +55 (61) 9 9209-6171>
     * @param Request $request
     * @param  int  $id
     * @return JsonResponse
     */
    public function update(Request $request, int $id): JsonResponse {
        try{
            $employe = Employe::where('employe_id', $id)->update([
                'employe_name' => $request->employe_name,
                'employe_birth' => $request->employe_birth,
            ]);
        }
        catch (Exception $exception) {
            report($exception);
            return response()->json($exception,500);
        }
        return response()->json($employe);
    }

    /**
     * Remove the specified resource from storage.
     * @author Gilson Vieira Castro Júnior <castrok@live.com / +55 (61) 9 9209-6171>
     * @param  int  $id
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse {
        try{
            $employe = Employe::destroy($id);
        }
        catch (Exception $exception) {
            report($exception);
            return response()->json($exception,500);
        }
        return response()->json($employe);
    }
}
