<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

class JobsController extends Controller {
    /**
     * Display a listing of the resource.
     * @author Gilson Vieira Castro Júnior <castrok@live.com / +55 (61) 9 9209-6171>
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     * @author Gilson Vieira Castro Júnior <castrok@live.com / +55 (61) 9 9209-6171>
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     * @author Gilson Vieira Castro Júnior <castrok@live.com / +55 (61) 9 9209-6171>
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     * @author Gilson Vieira Castro Júnior <castrok@live.com / +55 (61) 9 9209-6171>
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     * @author Gilson Vieira Castro Júnior <castrok@live.com / +55 (61) 9 9209-6171>
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     * @author Gilson Vieira Castro Júnior <castrok@live.com / +55 (61) 9 9209-6171>
     * @param Request $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @author Gilson Vieira Castro Júnior <castrok@live.com / +55 (61) 9 9209-6171>
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
