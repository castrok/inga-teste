<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class CustomersController extends Controller {
    /**
     * Display a listing of the resource.
     * @author Gilson Vieira Castro Júnior <castrok@live.com / +55 (61) 9 9209-6171>
     * @return JsonResponse
     */
    public function index(): JsonResponse {
        try {
            $customers = Customer::all();
        }
        catch (Exception $exception) {
            report($exception);
            return response()->json($exception,500);
        }
        return response()->json($customers);
    }

    /**
     * Store a newly created resource in storage.
     * @author Gilson Vieira Castro Júnior <castrok@live.com / +55 (61) 9 9209-6171>
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request) {
        try {
            $customer = Customer::insert([
                'customer_name' => $request->customer_name,
                'customer_address' => $request->customer_address,
            ]);
        }
        catch (Exception $exception) {
            report($exception);
            return response()->json($exception,500);
        }
        return response()->json($customer);
    }

    /**
     * Display the specified resource.
     * @param int $id
     * @return JsonResponse
     * @author Gilson Vieira Castro Júnior <castrok@live.com / +55 (61) 9 9209-6171>
     */
    public function show(int $id): JsonResponse {
        try{
            $customer = Customer::find($id);
        }
        catch (Exception $exception) {
            report($exception);
            return response()->json($exception,500);
        }
        return response()->json($customer);
    }

    /**
     * Return the specified resource for editing.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function edit(int $id): JsonResponse {
        try{
            $service = Customer::find($id);
        }
        catch (Exception $exception) {
            report($exception);
            return response()->json($exception,500);
        }
        return response()->json($service);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     * @author Gilson Vieira Castro Júnior <castrok@live.com / +55 (61) 9 9209-6171>
     */
    public function update(Request $request, int $id): JsonResponse {
        try{
            $customer = Customer::where('customer_id', $id)->update([
                'customer_name' => $request->customer_name,
                'customer_address' => $request->customer_address,
            ]);
        }
        catch (Exception $exception) {
            report($exception);
            return response()->json($exception,500);
        }
        return response()->json($customer);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return JsonResponse
     * @author Gilson Vieira Castro Júnior <castrok@live.com / +55 (61) 9 9209-6171>
     */
    public function destroy(int $id): JsonResponse {
        try{
            $customer = Customer::destroy($id);
        }
        catch (Exception $exception) {
            report($exception);
            return response()->json($exception,500);
        }
        return response()->json($customer);
    }
}
