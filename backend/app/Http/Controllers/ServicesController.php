<?php

namespace App\Http\Controllers;

use Exception;
use App\Models\Service;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

class ServicesController extends Controller {

    /**
     * Display a listing of the resource.
     * @author Gilson Vieira Castro Júnior <castrok@live.com / +55 (61) 9 9209-6171>
     * @return JsonResponse
     */
    public function index(): JsonResponse {
        /* User::insert([
             'name' => 'Gilson Vieira',
             'email' => 'castrok@live.com',
             'password' => Hash::make('teste@123'),
         ]);*/
        try {
            $services = Service::all();
        }
        catch (Exception $exception) {
            report($exception);
            return response()->json($exception,500);
        }
//        print '<pre>'; print_r(User::all()->toArray()); die;
        return response()->json($services);
    }

    /**
     * Store a newly created resource in storage.
     * @author Gilson Vieira Castro Júnior <castrok@live.com / +55 (61) 9 9209-6171>
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse {
        try {
            $service = Service::insert([
                'service_name' => $request->service_name,
                'service_description' => $request->service_description,
            ]);
        }
        catch (Exception $exception) {
            report($exception);
            return response()->json($exception,500);
        }
        return response()->json($service);
    }

    /**
     * Display the specified resource.
     * @param int $id
     * @return JsonResponse
     * @author Gilson Vieira Castro Júnior <castrok@live.com / +55 (61) 9 9209-6171>
     */
    public function show(int $id): JsonResponse {
        try{
            $service = Service::find($id);
        }
        catch (Exception $exception) {
            report($exception);
            return response()->json($exception,500);
        }
        return response()->json($service);
    }

    /**
     * Return the specified resource for editing.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function edit(int $id): JsonResponse {
        try{
            $service = Service::find($id);
        }
        catch (Exception $exception) {
            report($exception);
            return response()->json($exception,500);
        }
        return response()->json($service);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     * @author Gilson Vieira Castro Júnior <castrok@live.com / +55 (61) 9 9209-6171>
     */
    public function update(Request $request, int $id): JsonResponse {
        try{
            $service = Service::where('service_id', $id)->update([
                'service_name' => $request->service_name,
                'service_description' => $request->service_description,
            ]);
        }
        catch (Exception $exception) {
            report($exception);
            return response()->json($exception, 500);
        }
        return response()->json($service);
    }

    /**
     * Remove the specified resource from storage.
     * @author Gilson Vieira Castro Júnior <castrok@live.com / +55 (61) 9 9209-6171>
     * @param  int  $id
     * @return JsonResponse
     */
    public function destroy($id): JsonResponse {
        try{
            $service = Service::destroy($id);
        }
        catch (Exception $exception) {
            report($exception);
            return response()->json($exception,500);
        }
        return response()->json($service);
    }
}
