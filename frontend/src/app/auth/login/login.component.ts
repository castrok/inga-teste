import { Component, OnInit } from '@angular/core';
import {AuthService} from "../auth.service";
import {User} from "../user.model";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";

@Component({
  selector: 'inga-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user: User = new User();
  formGroup: FormGroup;
  response;
  errorCredentials: boolean = false;

  constructor(
      private authService: AuthService,
      private formBuilder: FormBuilder,
      private router: Router
  ) { }

  ngOnInit(): void {
    this.formGroup = this.formBuilder.group({
      email: [null, [Validators.required, Validators.email]],
      password: [null, [Validators.required]],
    });
  }

  login() {
    this.authService.login(this.user).subscribe(
        (response) => {
          this.response = response
          this.router.navigate(['/'])
        }
    )
  }

}
