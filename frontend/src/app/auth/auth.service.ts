import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {EMPTY, Observable} from "rxjs";
import {catchError, map, tap} from "rxjs/operators";
import {User} from "./user.model";
import {Router} from "@angular/router";

@Injectable({
    providedIn: 'root'
})

export class AuthService {
    baseurl: string = "http://localhost:8000/api";
    constructor(
        private http: HttpClient,
        private router: Router,
    ) { }

    errorHandler(erro: any):Observable<any> {
        alert('Falha ao realizar login !');
        return EMPTY
    }

    login(user: User): Observable<boolean> {
        return this.http.post<any>(`${this.baseurl}/login`, user).pipe(
            map((obj) => obj),
            tap(response => {
                    localStorage.setItem('token', response.access_token);
                    localStorage.setItem('user', btoa(JSON.stringify(response.user)));
                }
            ),
            catchError(error => this.errorHandler(error))
        );
    }

    logout(): void {
        localStorage.clear()
        this.router.navigate(['/login'])
        this.http.get<any>(`${this.baseurl}/logout`).pipe(
            map((obj) => obj),
            tap((response) => {
                console.log(response)
                    this.router.navigate(['/login'])
                }
            ),
            catchError(error => this.errorHandler(error))
        );
    }

    getUser() {
        return localStorage.getItem('user') ? JSON.parse(atob(localStorage.getItem('user'))) : null;
    }

    setUser(): Promise<boolean> {
        return this.http.get<any>(`${this.baseurl}/me`).toPromise()
            .then((response) => {
                if (response.user) {
                    localStorage.setItem('user', btoa(JSON.stringify(response.user)));
                    return true;
                }
                return false;
            })
    }

    check(): boolean {
        return !!localStorage.getItem('user');
    }
}
