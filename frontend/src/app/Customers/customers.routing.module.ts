import {RouterModule, Routes} from "@angular/router";
import {NgModule} from "@angular/core";
import {DestroyCustomersComponent} from "./destroy-customers/destroy-customers.component";
import {FormCustomersComponent} from "./form-customers/form-customers.component";
import {ShowCustomersComponent} from "./show-customers/show-customers.component";
import {IndexCustomersComponent} from "./index-customers/index-customers.component";
import {AuthGuard} from "../guards/auth.guard";

const appRoutes: Routes = [
    {path:'customers', component:IndexCustomersComponent, canActivate: [AuthGuard]},
    {path:'customers/create',component:FormCustomersComponent, canActivate: [AuthGuard]},
    {path:'customers/:id',component:ShowCustomersComponent, canActivate: [AuthGuard]},
    {path:'customers/:id/edit',component:FormCustomersComponent, canActivate: [AuthGuard]},
    {path:'customers/:id/destroy',component:DestroyCustomersComponent, canActivate: [AuthGuard]}
]

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes)
    ],
    exports: [
        RouterModule
    ],
})
export class CustomersRoutingModule { }
