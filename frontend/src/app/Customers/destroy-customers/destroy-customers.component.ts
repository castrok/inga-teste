import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {CustomersService} from "../customers.service";
import {Customers} from "../customers.model";

@Component({
  selector: 'inga-destroy-customers',
  templateUrl: './destroy-customers.component.html',
  styleUrls: ['./destroy-customers.component.css']
})
export class DestroyCustomersComponent implements OnInit {

  customers: Customers;
  id;

  constructor(
      private route: ActivatedRoute,
      private customerService: CustomersService,
      private router: Router,
  ) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params.id;
    this.customerService.edit(this.id).subscribe(customers => {
      this.customers = customers
    })
  }

  destroy() {
    this.customerService.destroy(this.id).subscribe(response => {
      if (response) {
        alert('Removido com sucesso !');
        this.router.navigate(['/customers'])
      }
    })
  }
}
