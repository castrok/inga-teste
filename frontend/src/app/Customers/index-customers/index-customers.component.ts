import { Component, OnInit } from '@angular/core';
import {Customers} from "../customers.model";
import {CustomersService} from "../customers.service";

@Component({
  selector: 'inga-index-customers',
  templateUrl: './index-customers.component.html',
  styleUrls: ['./index-customers.component.css']
})
export class IndexCustomersComponent implements OnInit {

  customers: Customers[];

  constructor(
      private customersService: CustomersService
  ) { }

  ngOnInit(): void {
    this.customersService.index().subscribe(customers => {
      this.customers = customers
    })
  }
}
