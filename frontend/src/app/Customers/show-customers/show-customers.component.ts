import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Customers} from "../customers.model";
import {CustomersService} from "../customers.service";



@Component({
  selector: 'inga-show-customers',
  templateUrl: './show-customers.component.html',
  styleUrls: ['./show-customers.component.css']
})
export class ShowCustomersComponent implements OnInit {

  customer: Customers;
  id;

  constructor(
      private route: ActivatedRoute,
      private customerService: CustomersService,
  ) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params.id;
    this.customerService.edit(this.id).subscribe(customer => {
      this.customer = customer
    })
  }
}
