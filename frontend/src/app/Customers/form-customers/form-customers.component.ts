import { Component, OnInit } from '@angular/core';
import {Customers} from "../customers.model";
import {ActivatedRoute, Router} from "@angular/router";
import {CustomersService} from "../customers.service";

@Component({
  selector: 'inga-form-customers',
  templateUrl: './form-customers.component.html',
  styleUrls: ['./form-customers.component.css']
})
export class FormCustomersComponent implements OnInit {

  customers: Customers = new Customers();

  constructor(
      private customersService: CustomersService,
      private router: Router,
      private route: ActivatedRoute
  ) {  }

  ngOnInit(): void {
    if(this.route.snapshot.params.id) {
      this.customersService.edit(this.route.snapshot.params.id).subscribe((customer) => {
        this.customers = customer;
      })
    }
  }

  persist() {
    if(this.route.snapshot.params.id) {

      this.customers.customer_id = this.route.snapshot.params.id;
      this.customersService.update(this.customers).subscribe((response) => {
        if(response) {
          this.router.navigate(['/customers'])
        }
      })
    }
    else {
      this.customersService.store(this.customers).subscribe((response) => {
        if(response) {
          this.router.navigate(['/customers'])
        }
      })
    }
  }

}
