import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {CustomersRoutingModule} from "./customers.routing.module";
import { ShowCustomersComponent } from './show-customers/show-customers.component';
import { IndexCustomersComponent } from './index-customers/index-customers.component';
import { FormCustomersComponent } from './form-customers/form-customers.component';
import { DestroyCustomersComponent } from './destroy-customers/destroy-customers.component';
import {FormsModule} from "@angular/forms";



@NgModule({
  declarations: [ShowCustomersComponent, IndexCustomersComponent, FormCustomersComponent, DestroyCustomersComponent],
  imports: [
    CommonModule,
    CustomersRoutingModule,
      FormsModule
  ],

  providers: [
    CustomersRoutingModule,
  ],

})
export class CustomersModule { }
