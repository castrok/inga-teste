import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {EMPTY, Observable} from "rxjs";
import {catchError, map} from "rxjs/operators";
import {Customers} from "./customers.model";

@Injectable({
  providedIn: 'root'
})

export class CustomersService {

  baseurl: string = "http://localhost:8000/api/customers";

  constructor(
      private http: HttpClient
  ) { }

  index(): Observable<Customers[]> {
    return this.http.get<Customers[]>(this.baseurl)
  }
  store(customers: Customers): Observable<Customers> {
    return this.http.post<Customers>(this.baseurl, customers).pipe(
        map((obj) => obj),
        catchError(error => this.errorHandler(error))
    );
  }
  errorHandler(erro: any):Observable<any> {
    alert('Ocorreu um erro!');
    return EMPTY
  }
  edit(customer_id: string): Observable<Customers> {
    const url = `${this.baseurl}/${customer_id}`
    return this.http.get<Customers>(url).pipe(
        map((obj) => obj),
        catchError(error => this.errorHandler(error))
    );
  }
  update (customers: Customers) :Observable<Customers>{
    const url = `${this.baseurl}/${customers.customer_id}`
    return this.http.put<Customers>(url, customers).pipe(
        map((obj) => obj),
        catchError(error => this.errorHandler(error))
    );
  }
  destroy(customer_id: number) :Observable<Customers> {
    const url = `${this.baseurl}/${customer_id}`
    return this.http.delete<Customers>(url).pipe(
        map((obj) => obj),
        catchError(error => this.errorHandler(error))
    );
  }
}
