import { Component } from '@angular/core';

@Component({
  selector: 'inga-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']

})
export class AppComponent {
  title = '';
}