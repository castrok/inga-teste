import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Employes} from './employes.model';
import {EMPTY, Observable} from "rxjs";
import {catchError, map} from "rxjs/operators";
import {Services} from "../Services/services.model";

@Injectable({
  providedIn: 'root'
})

export class EmployesService {

  baseurl: string = "http://localhost:8000/api/employes";

  constructor(
      private http: HttpClient
  ) { }

  index(): Observable<Employes> {
    return this.http.get<Employes>(this.baseurl)
  }
  store(data): Observable<Employes> {
    return this.http.post<any>(this.baseurl, data).pipe(
        map((obj) => obj),
        catchError(error => this.errorHandler(error))
    );
  }
  errorHandler(erro: any):Observable<any> {
    alert('Ocorreu um erro!');
    return EMPTY
  }
  edit(employe_id: number): Observable<Employes> {
    const url = `${this.baseurl}/${employe_id}`
    return this.http.get<Employes>(url).pipe(
        map((obj) => obj),
        catchError(error => this.errorHandler(error))
    );
  }
  update (employes: Employes) :Observable<Employes>{
    const url = `${this.baseurl}/${employes.employe_id}`
    return this.http.put<Employes>(url, employes).pipe(
        map((obj) => obj),
        catchError(error => this.errorHandler(error))
    );
  }
  destroy(employe_id: number) :Observable<Employes> {
    const url = `${this.baseurl}/${employe_id}`
    return this.http.delete<Employes>(url).pipe(
        map((obj) => obj),
        catchError(error => this.errorHandler(error))
    );
  }
}
