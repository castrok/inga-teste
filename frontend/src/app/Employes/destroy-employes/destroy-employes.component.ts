import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Employes} from "../employes.model";
import {EmployesService} from "../employes.service";

@Component({
  selector: 'inga-destroy-employes',
  templateUrl: './destroy-employes.component.html',
  styleUrls: ['./destroy-employes.component.css']
})
export class DestroyEmployesComponent implements OnInit {

  employe: Employes;
  id;

  constructor(
      private router: Router,
      private route: ActivatedRoute,
      private employesService: EmployesService
  ) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params.id;
    this.employesService.edit(this.id).subscribe(employes => {
      this.employe = employes
    })
  }

  destroy() {
    this.employesService.destroy(this.id).subscribe(response => {
      if (response) {
        alert('Removido com sucesso !');
        this.router.navigate(['/employes'])
      }
    })
  }

}
