import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Employes} from "../employes.model";
import {EmployesService} from "../employes.service";


@Component({
  selector: 'inga-form-employes',
  templateUrl: './form-employes.component.html',
  styleUrls: ['./form-employes.component.css']
})

export class FormEmployesComponent implements OnInit {

  employes: Employes = new Employes();

  constructor(
      private employesService: EmployesService,
      private router: Router,
      private route: ActivatedRoute
  ) {  }

  ngOnInit(): void {
    if(this.route.snapshot.params.id) {
      this.employesService.edit(this.route.snapshot.params.id).subscribe((employe) => {
        this.employes = employe;
      })
    }
  }

  persist() {
    if(this.route.snapshot.params.id) {

      this.employes.employe_id = this.route.snapshot.params.id;
      this.employesService.update(this.employes).subscribe((response) => {
        if(response) {
          this.router.navigate(['/employes'])
        }
      })
    }
    else {
      this.employesService.store(this.employes).subscribe((response) => {
        if(response) {
          this.router.navigate(['/employes'])
        }
      })
    }
  }
}
