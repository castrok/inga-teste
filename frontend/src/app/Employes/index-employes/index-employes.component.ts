import { Component, OnInit } from '@angular/core';
import {Employes} from "../employes.model";
import {EmployesService} from "../employes.service";

@Component({
  selector: 'inga-index-employes',
  templateUrl: './index-employes.component.html',
  styleUrls: ['./index-employes.component.css']
})
export class IndexEmployesComponent implements OnInit {

  employes: Employes

  constructor(
      private employesservice: EmployesService,
  ) { }
  ngOnInit(): void {
    this.employesservice.index().subscribe(employes => {
      this.employes = employes
      console.table(employes)
    })
  }

}
