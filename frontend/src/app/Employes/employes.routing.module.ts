import {RouterModule, Routes} from "@angular/router";
import {NgModule} from "@angular/core";
import {IndexEmployesComponent} from "./index-employes/index-employes.component";
import {FormEmployesComponent} from "./form-employes/form-employes.component";
import {ShowServiceComponent} from "../Services/show-service/show-service.component";
import {DestroyEmployesComponent} from "./destroy-employes/destroy-employes.component";
import {ShowEmployesComponent} from "./show-employes/show-employes.component";
import {AuthGuard} from "../guards/auth.guard";

const appRoutes: Routes = [
    {path:'employes', component:IndexEmployesComponent, canActivate: [AuthGuard]},
    {path:'employes/create',component:FormEmployesComponent, canActivate: [AuthGuard]},
    {path:'employes/:id',component:ShowEmployesComponent, canActivate: [AuthGuard]},
    {path:'employes/:id/edit',component:FormEmployesComponent, canActivate: [AuthGuard]},
    {path:'employes/:id/destroy',component:DestroyEmployesComponent, canActivate: [AuthGuard]}
]

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes)
    ],
    exports: [
        RouterModule
    ],
})
export class EmployesRoutingModule { }
