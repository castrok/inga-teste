import { Component, OnInit } from '@angular/core';

import {ActivatedRoute} from "@angular/router";
import {EmployesService} from "../employes.service";
import {Employes} from "../employes.model";


@Component({
  selector: 'inga-show-employes',
  templateUrl: './show-employes.component.html',
  styleUrls: ['./show-employes.component.css']
})
export class ShowEmployesComponent implements OnInit {

  employe: Employes;
  id;

  constructor(
      private route: ActivatedRoute,
      private employeService: EmployesService,
  ) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params.id;
    this.employeService.edit(this.id).subscribe((employe) => {
      this.employe = employe
    })
  }

}
