import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {EmployesService} from "./employes.service";
import {EmployesRoutingModule} from "./employes.routing.module";
import { DestroyEmployesComponent } from './destroy-employes/destroy-employes.component';
import { FormEmployesComponent } from './form-employes/form-employes.component';
import { IndexEmployesComponent } from './index-employes/index-employes.component';
import { ShowEmployesComponent } from './show-employes/show-employes.component';
import {FormsModule} from "@angular/forms";
import {CustomersService} from "../Customers/customers.service";

@NgModule({
  declarations: [
    DestroyEmployesComponent,
    FormEmployesComponent,
    IndexEmployesComponent,
    ShowEmployesComponent
  ],
  imports: [
    CommonModule,
    EmployesRoutingModule,
    FormsModule,
  ],
  providers: [
    EmployesService,
    CustomersService,
  ],
})
export class EmployesModule { }
