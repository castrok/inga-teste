import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {ServicesModule} from "./Services/services.module";
import {TopNavBarComponent} from "./template/top-nav-bar/top-nav-bar.component";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {AppRoutingModule} from "./app.routing.module";
import {EmployesModule} from "./Employes/employes.module";
import {CustomersModule} from "./Customers/customers.module";
import {AuthModule} from "./auth/auth.module";
import {AuthGuard} from "./guards/auth.guard";
import {TokenInterceptor} from "./interceptors/token.interceptor";

@NgModule({
  declarations: [
    AppComponent,
    TopNavBarComponent,
  ],

  imports: [
    BrowserModule,
    ServicesModule,
    EmployesModule,
    CustomersModule,
    HttpClientModule,
    AppRoutingModule,
    AuthModule,
  ],

  providers: [
    AuthGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true,
    },

  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
