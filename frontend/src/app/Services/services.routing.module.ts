import {RouterModule, Routes} from "@angular/router";
import {NgModule} from "@angular/core";
import {IndexServicesComponent} from "./index-services/index-services.component";
import {ShowServiceComponent} from "./show-service/show-service.component";
import {FormServicesComponent} from "./form-services/form-services.component";
import {DestroyServicesComponent} from "./destroy-services/destroy-services.component";
import {AuthGuard} from "../guards/auth.guard";

const appRoutes: Routes = [
    {path:'services', component:IndexServicesComponent, canActivate: [AuthGuard]},
    {path:'services/create',component:FormServicesComponent, canActivate: [AuthGuard]},
    {path:'services/:id',component:ShowServiceComponent, canActivate: [AuthGuard]},
    {path:'services/:id/edit',component:FormServicesComponent, canActivate: [AuthGuard]},
    {path:'services/:id/destroy',component:DestroyServicesComponent, canActivate: [AuthGuard]}
]

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes)
    ],
    exports: [
        RouterModule
    ],
})
export class ServicesRoutingModule { }
