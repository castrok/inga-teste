import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule} from "@angular/forms";
import {ServicesService} from "./services.service";
import {IndexServicesComponent} from "./index-services/index-services.component";
import {FormServicesComponent} from "./form-services/form-services.component";
import {DestroyServicesComponent} from "./destroy-services/destroy-services.component";
import {ShowServiceComponent} from "./show-service/show-service.component";
import {ServicesRoutingModule} from "./services.routing.module";

@NgModule({
  declarations: [
    IndexServicesComponent,
    FormServicesComponent,
    DestroyServicesComponent,
    ShowServiceComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ServicesRoutingModule
  ],
  providers: [
    ServicesService
  ],
  exports: [
    ServicesRoutingModule,
  ]
})
export class ServicesModule { }