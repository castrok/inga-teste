import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Services} from './services.model';
import {EMPTY, Observable} from "rxjs";
import {catchError, map} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})

export class ServicesService {

  baseurl: string = "http://localhost:8000/api/services";

  constructor(
      private http: HttpClient
  ) { }

  index(): Observable<Services[]> {
    return this.http.get<Services[]>(this.baseurl)
  }
  store(services: Services): Observable<Services> {
    return this.http.post<Services>(this.baseurl, services).pipe(
        map((obj) => obj),
        catchError(error => this.errorHandler(error))
    );
  }
  errorHandler(erro: any):Observable<any> {
    alert('Ocorreu um erro!');
    return EMPTY
  }
  edit(service_id: number): Observable<Services> {
    const url = `${this.baseurl}/${service_id}`
    return this.http.get<Services>(url).pipe(
        map((obj) => obj),
        catchError(error => this.errorHandler(error))
    );
  }

  update (services: Services) :Observable<Services>{
    const url = `${this.baseurl}/${services.service_id}`
    return this.http.patch<Services>(url, services).pipe(
        map((obj) => obj),
        catchError(error => this.errorHandler(error))
    );
  }

  destroy(service_id: number) :Observable<Services> {
    const url = `${this.baseurl}/${service_id}`
    return this.http.delete<Services>(url).pipe(
        map((obj) => obj),
        catchError(error => this.errorHandler(error))
    );
  }
}
