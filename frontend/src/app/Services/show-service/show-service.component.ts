import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Services} from "../services.model";
import {ServicesService} from "../services.service";

@Component({
  selector: 'inga-show-service',
  templateUrl: './show-service.component.html',
  styleUrls: ['./show-service.component.css']
})
export class ShowServiceComponent implements OnInit {

  service: Services;
  id;

  constructor(
      private route: ActivatedRoute,
      private serviceService: ServicesService,
  ) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params.id;
    this.serviceService.edit(this.id).subscribe(service => {
      this.service = service
    })
  }
}
