import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {ServicesService} from "../services.service";

@Component({
  selector: 'inga-destroy-services',
  templateUrl: './destroy-services.component.html',
  styleUrls: ['./destroy-services.component.css']
})
export class DestroyServicesComponent implements OnInit {

  services;
  id;

  constructor(
      private servicesService: ServicesService,
      private route:ActivatedRoute,
      private router: Router,
  ) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params.id;
    this.servicesService.edit(this.id).subscribe(services => {
      this.services = services
    })
  }

  destroy() {
    this.servicesService.destroy(this.id).subscribe(response => {
      if (response) {
        alert('Removido com sucesso !');
        this.router.navigate(['/services'])
      }
    })
  }
}
