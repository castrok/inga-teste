import { Component, OnInit } from '@angular/core';
import {Services} from "../services.model";
import {ServicesService} from "../services.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'inga-index-services',
  templateUrl: './index-services.component.html',
  styleUrls: ['./index-services.component.css']
})
export class IndexServicesComponent implements OnInit {

  services: Services[];

  constructor(
      private servicesService: ServicesService,
  ) { }

  ngOnInit(): void {
    this.servicesService.index().subscribe((services) => {
      this.services = services
      console.table(services)
    })
  }
}
