import { Component, OnInit } from '@angular/core';
import {Services} from "../services.model";
import {ServicesService} from "../services.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'inga-form-services',
  templateUrl: './form-services.component.html',
  styleUrls: ['./form-services.component.css']
})

export class FormServicesComponent implements OnInit {

  services: Services = new Services();

  constructor(
      private servicesService: ServicesService,
      private router: Router,
      private route: ActivatedRoute
  ) {  }

  ngOnInit(): void {
    if(this.route.snapshot.params.id) {
      this.servicesService.edit(this.route.snapshot.params.id).subscribe((service) => {
        this.services = service;
      })
    }
  }

  persist() {
    if(this.route.snapshot.params.id) {

      this.services.service_id = this.route.snapshot.params.id;
      this.servicesService.update(this.services).subscribe((response) => {
        if(response) {
          this.router.navigate(['/services'])
        }
      })
    }
    else {
      this.servicesService.store(this.services).subscribe((response) => {
        if(response) {
          this.router.navigate(['/services'])
        }
      })
    }
  }
}
